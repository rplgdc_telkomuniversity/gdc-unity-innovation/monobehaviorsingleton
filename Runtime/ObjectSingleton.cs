﻿using UnityEngine;

namespace GDC.Innovation.Singleton
{
    public abstract class ObjectSingleton<T> : MonoBehaviourSingleton<T> where T : MonoBehaviour
	{
		public static T Instance
		{
			get
			{
				if (!instance) instance = FindObjectOfType<T>();
				if (!instance && CreateInstanceIfNotPresent) instance = new GameObject(typeof(T).Name).AddComponent<T>();

				return instance;
			}
		}
	}
}
