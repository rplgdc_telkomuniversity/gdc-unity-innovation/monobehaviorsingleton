using UnityEngine;

namespace GDC.Innovation.Singleton
{
	public abstract class PrefabsSingleton<T> : MonoBehaviourSingleton<T> where T : MonoBehaviour
	{
		private static string rootPath = "PrefabsSingleton/";
		public static T Instance
		{
			get
			{
				if (!instance) instance = FindObjectOfType<T>();
				if (!instance && CreateInstanceIfNotPresent){
					 T temp = Resources.Load<T>(rootPath+typeof(T).Name);
					 instance = Instantiate<T>(temp);
				}

				return instance;
			}
		}
	}
}
