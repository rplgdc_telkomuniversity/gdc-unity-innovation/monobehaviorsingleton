using UnityEngine;
namespace GDC.Innovation.Singleton
{
	public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour
	{
		protected static T instance;
		public static bool IsInstanceExist => (instance != null);
		#region Config
		/// <summary>
		/// If true, create a new object if there is no instance in the scene.
		/// </summary>
		public static bool CreateInstanceIfNotPresent = true;
		#endregion


		protected virtual void Awake()
		{
			if (instance == null)
			{
				instance = GetComponent<T>();
			}

			DestroyDuplicates();
		}

		protected virtual void OnDestroy()
		{
			if (IsOtherComponentIs(thisGameObject: true))
			{
				instance = null;
			}
		}

		protected virtual void DestroyDuplicates()
		{
			if (IsOtherComponentIs(thisGameObject: false))
			{
				Destroy(gameObject);
			}
		}

		private bool IsOtherComponentIs(bool thisGameObject)
		{
			T component = GetComponent<T>();
			return component != null && (thisGameObject ? instance == component : instance != component);
		}
	}
}
